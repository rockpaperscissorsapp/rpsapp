**Title:** Rock Paper Scissors
**Group No:** Group 3
**Group Members:** Nyla Rashied, Nkem Ohanenye

**Sensors:** 
●	Image sensor for detecting and conveying information used to make an image.
●	Motion sensors to detect movement.
●	Touch sensor

**Goal:**
	There are many games made for people, but not many for the ones that want to play a game with someone, or an only child that just wants a friend. Our app for Rock Paper Scissors is crafted on this attempt to give a person someone to play with. The game will perform on an app and is played exactly how it is when you play within real life, however, you're going to compete with an AI. Outside of the game menu, there will be a picture of the character that you can interact with by tapping him (not physically talking, just some words strung up in a string array and randomly picked). If time will allow, we will add different versions of the Rock Paper Scissors game like, without a camera touch-based game, or a game where you always win. An addition that can be added to the inserted character is that it will notice when the phone is moved harshly and will react with other strings, telling the user “oww, be more careful,” or something to that regard.

**Application Scenario:**     
	The target audience for this application will be people that just want to spend their time playing a game with someone else who also interacts with them. While there are several game applications that exist, we wanted to create one that helped the people that were more lonely. Or even someone who always wants to win in a match. Our game will help the player to never rely on chance to win but to incorporate some strategy. 
